/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.majora.minecraft.blockdispenser.listeners.BlockListener;
import com.majora.minecraft.blockdispenser.listeners.PlayerListener;
import com.majora.minecraft.blockdispenser.locale.Locale;
import com.majora.minecraft.blockdispenser.models.IRepository;
import com.majora.minecraft.blockdispenser.models.JSONRepository;

public class BlockDispenser extends JavaPlugin
{
	public static Logger consoleLogger = Logger.getLogger( "Minecraft" );
	private static BlockDispenser instance = null;
	public static String prefix;
	
	private BlockListener	blockListener;
	private PlayerListener playerListener;
	
	private IRepository<Location, DispenserState> repository;
	public BukkitTask backupTaskID;
	
	private Locale locale;
	
	protected static volatile String 		databaseFileName = "database.json";
	protected FileConfiguration				dispenserConfig = null;
	
	boolean										useColorTags;
	boolean 									usePermissions;
	int											backupTime;

	public void onEnable()
	{
		instance = this;
		
		// Save a copy of the default config.yml if one is not there
        this.saveDefaultConfig();
        
        initializeLoggerPrefix();
        
        final String dispensersFilePath = this.getDataFolder() + "\\" + databaseFileName;
		repository = new JSONRepository( dispensersFilePath, getServer() );
        
		
		
		// Load all configuration data
		loadProperties(false);
		
		locale = new Locale( this, getConfig().getString( "options.locale" ) );
		
		// Load repository
		loadIfFileExitsts( dispensersFilePath );
		
		// Register listeners
		createAndRegisterListeners();
		
		// Schedule Background Tasks
		startBackgroundBackupTask();


		if ( isDebug() )
		{
			BlockDispenser.log( locale.getValue( "Plugin.DebugMode" ), null );
		}
		
		//Bukkit.getConsoleSender().sendMessage( color("green", TAG + locale.getValue( "Plugin.PluginEnabled" )) ); // This is ensured to be green
		BlockDispenser.log( locale.getValue( "Plugin.PluginEnabled" ), "green" );
	}
	
	public void onDisable()
	{
		saveDefaultConfig();
		
		stopBackgroundBackupTasks();
		
		repository.save();
		
		/*if( saveDispensers( databaseFileName ) )
		{
			dispensers.clear();
		} else {
			consoleLogger.severe( color ("red", TAG + locale.getValue("File.CouldNotSaveDispensers")) );
		}*/
		
		databaseFileName = null;
		
		BlockDispenser.log( locale.getValue( "Plugin.PluginDisabled" ), null );
	}
	
	private void initializeLoggerPrefix()
	{
		final PluginDescriptionFile pluginDescriptionFile = getDescription();
		prefix = "[" +  pluginDescriptionFile.getName() + "]: ";
	}
	
	public boolean isDebug()
	{
		if (getConfig() == null) return false;
		else return getConfig().getBoolean( "options.debug" );
	}

	private void startBackgroundBackupTask()
	{
		if (backupTime > 0)
		{
			backupTaskID = createAndStartAsyncBackgroundThread();
		}
	}

	private BukkitTask createAndStartAsyncBackgroundThread()
	{
		return this.getServer().getScheduler().runTaskTimerAsynchronously( this, new Runnable() {
			 public void run() 
			   {
				   if (getServer().getOnlinePlayers().length == 0)
				   {
					   String t = prefix + locale.getValue( "Backup.BackupNoPlayers" );
					   Bukkit.getConsoleSender().sendMessage(color("gray", BlockDispenser.prefix + locale.format( t, backupTime )) );

				   } else
				   {
					   Bukkit.getConsoleSender().sendMessage(color("gray", BlockDispenser.prefix + locale.getValue( "Backup.BackupBegin" )) );
				       
				       repository.save();
				       Bukkit.getConsoleSender().sendMessage( color("gray", BlockDispenser.prefix + locale.getValue( "Backup.BackupEnd" )) );
				   }
			       
			   }
		}, backupTime * 20*60, backupTime * 20*60 );
	}
	
	private void stopBackgroundBackupTasks()
	{
		if (backupTime > 0)
		{
			backupTaskID.cancel();
		}
	}

	public Locale getLocale()
	{ 
		return locale;
	}

	private void createAndRegisterListeners()
	{
		blockListener	= new BlockListener(this, repository);
        playerListener = new PlayerListener(this, repository);
        
		this.getServer().getPluginManager().registerEvents( this.blockListener, this );
		this.getServer().getPluginManager().registerEvents( this.playerListener, this );
	}

	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!cmd.getName().equalsIgnoreCase( "blockdispenser" ) || !cmd.getName().equalsIgnoreCase( "bd" )) return false;
		if (sender instanceof Player) return true;
		
		return true;
	}
    
    protected void loadProperties(boolean reload)
    {	
    	if (reload)
    	{
    		this.reloadConfig();
    	}
    	
    	boolean propChanged = true;
    	
    	useColorTags = getConfig().getBoolean( "options.enable-color-tags" );
    	usePermissions = getConfig().getBoolean( "options.use-persissions" );
    	backupTime = getConfig().getInt( "options.backup-time" );
    	
    	if (propChanged)
    	{
    		this.saveConfig();
    	}
    	
    	if (backupTime < 0) backupTime = 0;
    	
    	if ( reload )
    	{
    		repository.save(); // TODO: Have this run from an async thread
    	}
    	
    	loadIfFileExitsts(databaseFileName); // TODO: Have this run from an sync thread
    }
    
    
    
    ////////////////////////////////////////////////////////////////////////////////////
    //					Utility Functions
    //
    
    private void loadIfFileExitsts(final String dispensersFilePath) {
		if (com.majora.minecraft.blockdispenser.utils.FileUtils.exists(dispensersFilePath))
		{
			repository.load();
		} else {
			BlockDispenser.log("No vaults file loaded.", null);
		}
	}
	

	/**
	 * Initialise the data directory for this plugin.
	 * 
	 * @return true if the directory has been created or already exists.
	 */
	private boolean createDataDirectory()
	{
		File file = this.getDataFolder();
		
		if ( !file.isDirectory() )
		{
			if ( !file.mkdirs() )
			{
				// failed to create the non existent directory, so failed
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Retrieve a File description of a data file for your plugin. This file
	 * will be looked for in the data directory of your plugin, wherever that
	 * is. There is no need to specify the data directory in the filename such
	 * as "plugin/datafile.dat" Instead, specify only "datafile.dat"
	 * 
	 * @param filename
	 *            The name of the file to retrieve.
	 * @param mustAlreadyExist
	 *            True if the file must already exist on the filesystem.
	 * 
	 * @return A File descriptor to the specified data file, or null if there
	 *         were any issues.
	 */
	private File getDataFile( String filename, boolean mustAlreadyExist )
	{
		if ( createDataDirectory() )
		{
			File file = new File( this.getDataFolder(), filename );
			if ( mustAlreadyExist )
			{
				if ( file.exists() ) { return file; }
			} else
			{
				return file;
			}
		}
		return null;
	}
	
	public static void logSevere(final String msg, String color)
	{
		if (color == null) {
			BlockDispenser.consoleLogger.severe(prefix + msg);
		} else {
			BlockDispenser.consoleLogger.severe(instance.color(color, prefix + msg));
		}
	}
	
	public static void log(final String msg, String color)
	{
		if (color == null) {
			BlockDispenser.consoleLogger.info(prefix + msg);
		} else {
			BlockDispenser.consoleLogger.info(instance.color(color, prefix + msg));
		}
		
	}
	
	public String color(String color, String message)
	{
		if (!getConfig().getBoolean( "options.enable-colors" )) return message; 
		
		String colorPrefix = "";
		
		if (color.equalsIgnoreCase( "GREEN" )) {
			colorPrefix = ChatColor.GREEN + "";
		} else if (color.equalsIgnoreCase( "BLUE" )) {
			colorPrefix = ChatColor.BLUE + "";
		} else if (color.equalsIgnoreCase( "RED" )) {
			colorPrefix = ChatColor.RED + "";
		} else if (color.equalsIgnoreCase( "BLACK" )) {
			colorPrefix = ChatColor.BLACK + "";
		} else if (color.equalsIgnoreCase( "DARK BLUE" )) {
			colorPrefix = ChatColor.DARK_BLUE + "";
		} else if (color.equalsIgnoreCase( "DARK GREEN" )) {
			colorPrefix = ChatColor.DARK_GREEN + "";
		} else if (color.equalsIgnoreCase( "AQUA" )) {
			colorPrefix = ChatColor.AQUA + "";
		} else if (color.equalsIgnoreCase( "WHITE" )) {
			colorPrefix = ChatColor.WHITE + "";
		} else if (color.equalsIgnoreCase( "GRAY" ) || color.equalsIgnoreCase( "GREY" )) {
			colorPrefix = ChatColor.GRAY + "";
		} else if (color.equalsIgnoreCase( "GOLD" )) {
			colorPrefix = ChatColor.GOLD + "";
		}
		
		return colorPrefix + message;
	}
}