/**
 * Distributed under the The Non-Profit Open Software License version 3.0 (NPOSL-3.0)
 * http://www.opensource.org/licenses/NOSL3.0
 */
package com.majora.minecraft.blockdispenser;

import java.io.Serializable;

/**
 * @author jvmilazz
 *
 */
public class DispenserState implements Serializable
{
	private static final long	serialVersionUID	= 1L;
	
	private DispenserAction dispenserType = DispenserAction.NORMAL;
	private DispenserAction prevState = DispenserAction.NORMAL;
	private DispenserAction currentState = DispenserAction.NORMAL;
	
	public DispenserState(DispenserAction dispenserType, DispenserAction defaultState ) {
		this.dispenserType = dispenserType;
		this.currentState = defaultState;
		
		if (dispenserType == DispenserAction.BOTH)
		{
			this.currentState = DispenserAction.EJECT;
			this.prevState = DispenserAction.REMOVE;
		} else {
			this.prevState = currentState;
		}
		
	}
	
	public void toggleState()
	{
		DispenserAction temp = prevState;
		prevState = currentState;
		currentState = temp;
	}

	/**
	 * @return the prevState
	 */
	public DispenserAction getPrevState()
	{
		return this.prevState;
	}

	/**
	 * @param prevState the prevState to set
	 */
	public void setPrevState( DispenserAction prevState )
	{
		this.prevState = prevState;
	}

	/**
	 * @return the currentState
	 */
	public DispenserAction getCurrentState()
	{
		return this.currentState;
	}

	/**
	 * @param currentState the currentState to set
	 */
	public void setCurrentState( DispenserAction currentState )
	{
		this.currentState = currentState;
	}

	/**
	 * @return the dispenserType
	 */
	public DispenserAction getDispenserType()
	{
		return this.dispenserType;
	}

	/**
	 * @param dispenserType the dispenserType to set
	 */
	public void setDispenserType( DispenserAction dispenserType )
	{
		this.dispenserType = dispenserType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ( ( this.currentState == null ) ? 0 : this.currentState
						.hashCode() );
		result = prime
				* result
				+ ( ( this.dispenserType == null ) ? 0 : this.dispenserType
						.hashCode() );
		result = prime * result
				+ ( ( this.prevState == null ) ? 0 : this.prevState.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj )
	{
		if ( this == obj ) { return true; }
		if ( obj == null ) { return false; }
		if ( ! ( obj instanceof DispenserState ) ) { return false; }
		DispenserState other = (DispenserState) obj;
		if ( this.currentState != other.currentState ) { return false; }
		if ( this.dispenserType != other.dispenserType ) { return false; }
		if ( this.prevState != other.prevState ) { return false; }
		return true;
	}
	
	
	
	
	
}
