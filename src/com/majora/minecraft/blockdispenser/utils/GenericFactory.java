/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author jvmilazz
 *
 */
public class GenericFactory
{
	public static <K, V> HashMap<K, V> newHashMap() {
		return new HashMap<K, V>();
	}
	
	public static <E> ArrayList<E> newArrayList() {
		return new ArrayList<E>();
	}
	
}
