/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.utils;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

/**
 * @author jvmilazz
 *
 */
public final class Utility
{
	public static BlockFace getBlockFace(Block block) 
	{
		byte blockData = block.getData();
		
		switch (blockData)
		{
			case (0):
				return BlockFace.DOWN;
			case (1):
				return BlockFace.UP;
			case (2):
				return BlockFace.NORTH;
			case (3):
				return BlockFace.SOUTH;
			case (4):
				return BlockFace.EAST;
			case (5):
				return BlockFace.WEST;
			case (8):
				return BlockFace.DOWN;
			case (9):
				return BlockFace.UP;
			case (10):
				return BlockFace.NORTH;
			case (11):
				return BlockFace.SOUTH;
			case (12):
				return BlockFace.EAST;
			case (13):
				return BlockFace.WEST;
			default:
				return null;
		}
	}
	
	public static void setMetadata(Player player, String key, Object value, Plugin plugin) 
	{
	  player.setMetadata(key,new FixedMetadataValue(plugin,value));
	}
	
	public static Object getMetadata(Player player, String key, Plugin plugin) 
	{
	  List<MetadataValue> values = player.getMetadata(key);  
	  
	  for(MetadataValue value : values)
	  {
	     if(value.getOwningPlugin().getDescription().getName().equals(plugin.getDescription().getName()))
	     {
	        return value.value();
	     }
	  }
	  return null;
	}
	
}
