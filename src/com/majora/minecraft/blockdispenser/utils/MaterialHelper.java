/**
 * Distributed under the The Non-Profit Open Software License version 3.0 (NPOSL-3.0)
 * http://www.opensource.org/licenses/NOSL3.0
 */
package com.majora.minecraft.blockdispenser.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public final class MaterialHelper
{
	public static boolean isSapling(Material mat)
	{
		if (mat == Material.SAPLING) return true;
		else return false;
	}
	public static boolean isSapling(int itemID)
	{
		return isSapling( Material.getMaterial( itemID ) );
	}
	
	public static boolean isItem(Material mat) 
	{
		if (mat.isBlock()) return false;
		else return true;
	}
	public static boolean isItem(int itemID) 
	{
		if (Material.getMaterial( itemID ).isBlock()) return false;
		else return true;
	}
	
	public static boolean isBlock(Material mat) 
	{
		return mat.isBlock();
	}
	public static boolean isBlock(int itemID) 
	{
		return isBlock(Material.getMaterial( itemID ));
	}
	
	public static boolean isContainer(Material mat) 
	{
		if (isBlock(mat))
		{
			if (mat == Material.BREWING_STAND || mat == Material.BURNING_FURNACE || mat == Material.CAULDRON || mat == Material.CHEST || mat == Material.DISPENSER || mat == Material.ENCHANTMENT_TABLE || mat == Material.FURNACE || mat == Material.JUKEBOX || mat == Material.LOCKED_CHEST || mat == Material.POWERED_MINECART || mat == Material.STORAGE_MINECART || mat == Material.WORKBENCH)
			{
				return true;
			}
		}
		
		return false;
	}
	public static boolean isContainer(int itemID) 
	{
		Material mat = Material.getMaterial( itemID );
		
		return isContainer( mat );
	}
	
	public static boolean isAir(Material mat) 
	{
		return (mat == Material.AIR);
	}
	public static boolean isAir(int itemID) 
	{
		return isAir( Material.getMaterial( itemID ) );
	}
	
	/**
	 * Checks whether Material is natural or a plant. </p> This does not mean <code>bonemeal</code> can be used on it. Use <code>MaterialHelper.canUseBonemealOn(Material mat)</code> instead.
	 * @param mat
	 * @return
	 */
	public static boolean isPlant(Material mat)
	{
		if (mat == Material.CACTUS || mat == Material.CROPS || mat == Material.DEAD_BUSH || mat == Material.MELON_STEM || mat == Material.PUMPKIN_STEM || mat == Material.SAPLING || mat == Material.SEEDS || mat == Material.SUGAR_CANE_BLOCK || mat == Material.VINE || mat == Material.WATER_LILY || mat == Material.WHEAT || mat == Material.RED_ROSE || mat == Material.RED_MUSHROOM || mat == Material.BROWN_MUSHROOM || mat == Material.YELLOW_FLOWER)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean isPlant(int itemID)
	{
		return isPlant(Material.getMaterial( itemID ));
	}
	
	public static boolean isNaturallyStackable(Material mat) {return false;}
	public static boolean isNaturallyStackable(int itemID) {return false;}
	
	/* Below are functions just for BlockDispenser */
	
	/**
	 * Checks if the Material is empty in which a block can be placed on it or an entity 
	 * spawned. 
	 * 
	 * @param Material ID
	 * @return 
	 */
	public static boolean isEmptyBlock(Material mat)
	{
		if (mat == Material.AIR || mat == Material.WATER || mat == Material.STATIONARY_WATER || mat == Material.LAVA || mat == Material.STATIONARY_LAVA || mat == Material.POWERED_RAIL || mat == Material.DETECTOR_RAIL || mat == Material.RAILS || mat == Material.GRASS || mat == Material.DEAD_BUSH )
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean isEmptyBlock(int itemID)
	{
		return isEmptyBlock( Material.getMaterial( itemID ) );
	}
	
	/**
	 * Checks if Material is going to be handled in a special way by BlockDispenser.
	 * @param mat
	 * @return
	 */
	public static boolean isSpecialPlaceables(Material mat)
	{
		if ( mat == Material.WATER_BUCKET || mat == Material.LAVA_BUCKET || mat == Material.MINECART || mat == Material.STORAGE_MINECART || mat == Material.POWERED_MINECART || mat == Material.RAILS || mat == Material.POWERED_RAIL || mat == Material.DETECTOR_RAIL)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean isSpecialPlaceables(int itemID)
	{
		return isSpecialPlaceables( Material.getMaterial( itemID ) );
	}
	
	/**
	 * Checks whether bonemeal can be used on a Material.
	 * @param mat
	 * @return
	 */
	public static boolean canUseBonemealOn(Material mat)
	{
		if (mat == Material.CROPS || mat == Material.WHEAT || mat == Material.CACTUS || mat == Material.SAPLING || mat == Material.DIRT || mat == Material.GRASS || mat == Material.RED_MUSHROOM || mat == Material.BROWN_MUSHROOM || mat == Material.SUGAR_CANE_BLOCK || mat == Material.MELON_STEM || mat == Material.PUMPKIN_STEM || mat == Material.POTATO || mat == Material.CARROT)
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean canUseBonemealOn(int itemID)
	{
		return canUseBonemealOn( Material.getMaterial( itemID ) );
	}
	
	public static boolean isBonemeal(ItemStack item)
	{
		if (item.getType() == Material.INK_SACK && item.getDurability() == 15 /*Bonemeal*/)
		{
			return true;
		}
		
		return false;
	}
}
