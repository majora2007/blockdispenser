/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.utils;

import java.io.File;

/**
 * @author jvmilazz
 *
 */
public final class FileUtils
{
	public static boolean exists(final String file)
	{
		return new File(file).exists();
	}
}
