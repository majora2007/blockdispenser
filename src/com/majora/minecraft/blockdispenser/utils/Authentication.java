/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.utils;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.yi.acru.bukkit.Lockette.Lockette;

import com.majora.minecraft.blockdispenser.BlockDispenser;

/**
 * @author jvmilazz
 *
 */
public final class Authentication {
	
	public static boolean hasPermission(final CommandSender sender, final String permission) {
		if (sender instanceof Player)
		{
			return hasPermission(((Player) sender), permission);
		}
		
		// If not a player, then it must be the console which always has permission.
		return true;
	}
	
	public static boolean hasPermission(final Player player, final String perm)
	{
		boolean result = false;
		
		if (perm.equals("blockdispenser.set"))
		{
			result = player.hasPermission("blockdispenser.admin.set") || player.hasPermission("blockdisepenser.set");
		} else if (perm.equals("blockdispenser.state")) {
			result = player.hasPermission("blockdispenser.admin") || player.hasPermission("blockdispenser.state");
		} else if (perm.equals("blockdispenser.force-save")) {
			result = player.hasPermission("blockdispenser.admin") || player.hasPermission("blockdispenser.force-save");
		} else if (perm.equals("blockdispenser.clear")) {
			result = player.hasPermission("blockdispenser.admin") || player.hasPermission("blockdispenser.clear");
		} else if (perm.equals("blockdispenser.admin.reload")) {
			result = player.hasPermission("blockdispenser.admin.reload");
		} else if (perm.equals("blockdispenser.state")) {
			result = player.hasPermission("blockdispenser.admin") || player.hasPermission("blockdispenser.state");
		} else if (perm.equals("experienceshelves.move")) {
			result = player.hasPermission("experienceshelves.move");
		}
		
		if (!result) {
			player.sendMessage(ChatColor.RED + "You do not have permission to do that.");
		}
		
		return result;
	}
	
	public static boolean isBlockLocketteProtected( Player player, BlockDispenser plugin )
	{
		Plugin lockettePlugin = plugin.getServer().getPluginManager().getPlugin( "Lockette" );
		if (lockettePlugin != null)
		{
			Block selectedBlock = ((Dispenser) Utility.getMetadata( player, "bd_selected_dispenser", plugin )).getBlock();
			return (Lockette.isUser(selectedBlock, player.getName(), true ));
		}
		
		return false;
	}

}
