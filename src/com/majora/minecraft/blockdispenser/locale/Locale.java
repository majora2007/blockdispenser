/**
 * Distributed under the The Non-Profit Open Software License version 3.0 (NPOSL-3.0)
 * http://www.opensource.org/licenses/NOSL3.0
 */
package com.majora.minecraft.blockdispenser.locale;

import com.majora.minecraft.blockdispenser.BlockDispenser;

public class Locale
{
	private CustomConfig langYAML;
	private String language;
	private BlockDispenser plugin;
	
	public Locale(BlockDispenser plugin, String language)
	{
		this.plugin = plugin;
		this.language = language;
		this.langYAML = getLanguageConfig();
		this.langYAML.saveDefaultConfig();
	}
	
	public String getLanguage()
	{
		return this.language;
	}
	
	public String getValue(String key)
	{
		return langYAML.getCustomConfig().getString( key );
	}
	
	public String format(String message, Object ... args)
	{
		final String replaceToken = "{}";
		StringBuilder formattedMessage = new StringBuilder();
		
		if (args.length == 0) return message;
		if (!message.contains( replaceToken )) return message;
		
		int nextDelimiter = 0; 
		int previousDelemiter = 0;
		int argsIndex = 0;
		String tempString = message;
		
		while (argsIndex < args.length && tempString.contains( replaceToken ))
		{
			previousDelemiter = nextDelimiter;
			tempString = message.substring( argsIndex == 0 ? 0 : previousDelemiter + 2, message.length() );
			
			nextDelimiter = findIndexOfChar(tempString, replaceToken);
			
			if (nextDelimiter == 0 && argsIndex == args.length - 1)
			{
				formattedMessage.append( args[argsIndex] );
			} else {
				formattedMessage.append( tempString.substring( 0, nextDelimiter < message.length() ? nextDelimiter : message.length() ) );
				formattedMessage.append( args[argsIndex] );
			}
			
			argsIndex++;
		}
		
		String remainingMessage = tempString.substring( nextDelimiter + 2 < tempString.length() ? nextDelimiter + 2 : tempString.length(), tempString.length()  );
		
		if (remainingMessage.length() > 0) // Add the remaining characters from message
		{

			formattedMessage.append( remainingMessage );
		}
		
		return formattedMessage.toString();
	}
	
	
	
	private int findIndexOfChar(String message, String replaceSeq)
	{
		int index = 0;
		
		index = message.indexOf( replaceSeq );
		
		if (message.charAt( index + 1 ) == '}')
			return index;
		else 
			index += findIndexOfChar(message.substring( index + 1 ), replaceSeq);
		
		return index;
	}
	
	
	private CustomConfig getLanguageConfig()
	{
		String languageConfigFilePath = getLanguageConfigFilePath();
		return langYAML = new CustomConfig( plugin, languageConfigFilePath );	
	}

	private String getLanguageConfigFilePath()
	{
		return "languages/" + language.toLowerCase() + ".yml";
	}
	
}
