/**
 * Distributed under the The Non-Profit Open Software License version 3.0 (NPOSL-3.0)
 * http://www.opensource.org/licenses/NOSL3.0
 */
package com.majora.minecraft.blockdispenser.locale;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.majora.minecraft.blockdispenser.BlockDispenser;

/**
 * @author Majora2007
 *
 */
public class CustomConfig
{
	BlockDispenser plugin;
	protected static FileConfiguration customConfig = null;
	protected File customConfigFile = null;
	private String configFileName = "messages.yml";
	
	public CustomConfig(BlockDispenser plugin, String customConfigFileName) {
		this.plugin = plugin;
		this.configFileName = customConfigFileName;
		
		reloadCustomConfig();
	}
	
	public void reloadCustomConfig() 
	{
		if (customConfigFile == null)
		{
			customConfigFile = new File(plugin.getDataFolder(), configFileName);
		}
		customConfig = YamlConfiguration.loadConfiguration( customConfigFile );
		
		// Look for defaults in the jar
		InputStream defaultConfigStream = plugin.getResource( configFileName );
		if (defaultConfigStream != null)
		{
			YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration( defaultConfigStream );
			customConfig.setDefaults( defaultConfig );
		}
	}
	
	public FileConfiguration getCustomConfig()
	{
		if (customConfig == null)
		{
			plugin.reloadConfig();
		}
		return customConfig;
	}
	
	public void saveCustomConfig() 
	{
		if (customConfig == null || customConfigFile == null)
		{
			return;
		} 
		try {
			getCustomConfig().save(customConfigFile);
		} catch (IOException ex) {
			plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
		}
	}
	
	public void saveDefaultConfig() {
		if (!customConfigFile.exists()) {
			plugin.saveResource( configFileName, false );
		}
	}
	
}
