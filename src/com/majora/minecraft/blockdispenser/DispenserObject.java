/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser;

import org.bukkit.block.BlockFace;
import org.bukkit.block.Dispenser;

/**
 * @author jvmilazz
 *
 */
public class DispenserObject
{
	Dispenser dispenser;
	BlockFace dispenserFace;
	
	public DispenserObject() {
		
	}

	/**
	 * @return the dispenser
	 */
	public Dispenser getDispenser()
	{
		return this.dispenser;
	}

	/**
	 * @param dispenser the dispenser to set
	 */
	public void setDispenser( Dispenser dispenser )
	{
		this.dispenser = dispenser;
	}

	/**
	 * @return the dispenserFace
	 */
	public BlockFace getDispenserFace()
	{
		return this.dispenserFace;
	}

	/**
	 * @param dispenserFace the dispenserFace to set
	 */
	public void setDispenserFace( BlockFace dispenserFace )
	{
		this.dispenserFace = dispenserFace;
	}
	
	
	
}
