/**
 * Distributed under the The Non-Profit Open Software License version 3.0 (NPOSL-3.0)
 * http://www.opensource.org/licenses/NOSL3.0
 */
package com.majora.minecraft.blockdispenser;

public enum DispenserAction
{
	NORMAL,
	REMOVE,
	EJECT,
	BOTH;
	
	public static DispenserAction convertToDispenserAction(final String action)
	{
		if (action.equals( NORMAL.toString() ))
			return NORMAL;
		else if (action.equals( REMOVE.toString() ))
			return REMOVE;
		else if (action.equals( EJECT.toString() ))
			return EJECT;
		else if (action.equals( BOTH.toString() ))
			return BOTH;
		else return null;
	}
}
