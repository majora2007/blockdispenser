/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.models;

import java.util.Map;

/**
 * @author jvmilazz
 *
 */
public interface IRepository<K, V>
{
	void save();
	void load();
	
	void put(K key, V value);
	V get(K key);
	V remove(K key);
	boolean containsKey(K key);
	boolean isEmpty();
	
	Map<K, V> getData();
}
