/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.models;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.majora.minecraft.blockdispenser.DispenserAction;
import com.majora.minecraft.blockdispenser.DispenserState;
import com.majora.minecraft.blockdispenser.utils.GenericFactory;

/**
 * @author jvmilazz
 *
 */
public class JSONRepository implements IRepository<Location, DispenserState>
{
	private Map<Location, DispenserState> dispensers;
	
	private Server server = null;
	private String filePath = null;
	
	public JSONRepository(String filePath, Server server) {
		this.filePath = filePath;
		this.server = server;
		
		dispensers = GenericFactory.newHashMap();
	}

	@Override
	public void save()
	{
		JSONObject database = new JSONObject();
		JSONArray allDispenserStates = new JSONArray();
		
		for(Location loc : dispensers.keySet())
    	{
    		Iterator<Entry<Location, DispenserState>> it = dispensers.entrySet().iterator();
        	JSONObject dispenserState = new JSONObject();
        	
        	while (it.hasNext())
        	{
        		Entry<Location, DispenserState> pairs = it.next();
        		
        		// Create a JSONObject for the location
        		JSONObject dispenserLocation = new JSONObject();
        		dispenserLocation.put( "x", new Double(pairs.getKey().getBlockX()) );
        		dispenserLocation.put( "y", new Double(pairs.getKey().getBlockY()) );
        		dispenserLocation.put( "z", new Double(pairs.getKey().getBlockZ()) );
        		dispenserLocation.put( "world", pairs.getKey().getWorld().getName() );
        		
        		dispenserState.put( "location", dispenserLocation );
        		dispenserState.put( "type", pairs.getValue().getDispenserType().toString() );
        		dispenserState.put( "current action", pairs.getValue().getCurrentState().toString() );
        	}
        	
        	allDispenserStates.add( dispenserState );
    	}
		
		database.put( "dispenser states", allDispenserStates );
		
		writeToDisk(database);
	}

	@Override
	public void load()
	{
		JSONParser parser = new JSONParser(); // I only need one instance of this, I should move all parsing/saving into a class (reload will cause mem leak)
		try
		{
			Object object = parser.parse( new FileReader(filePath) );
			JSONObject jsonObject = (JSONObject) object;
			
			// For each DisepenserState in jsonObject, we will getLocation from Bukkit and construct a new DispenserState and add to Map.
			JSONArray dispenserStates = (JSONArray) jsonObject.get( "dispenser states" );
			
			for (int dispenserStateIndex = 0; dispenserStateIndex < dispenserStates.size(); dispenserStateIndex++)
			{
				JSONObject dispenserState = (JSONObject) dispenserStates.get(dispenserStateIndex);
				Location loc = extractAndCreateLocation( dispenserState );
				DispenserState dispState = extractDispenserState( dispenserState );
				
				put(loc, dispState);
			}
			
			parser = null;
			
			} catch ( ParseException ex )
			{
				ex.printStackTrace();
			} catch ( FileNotFoundException ex )
			{
				ex.printStackTrace();
			} catch ( IOException ex )
			{
				ex.printStackTrace();
			}
		
			parser = null;
		
	}

	@Override
	public void put( Location location, DispenserState dispenserState )
	{
		dispensers.put( location, dispenserState );
	}

	@Override
	public DispenserState get( Location location )
	{
		return dispensers.get( location );
	}

	@Override
	public DispenserState remove( Location location )
	{
		return dispensers.remove( location );
	}

	@Override
	public boolean containsKey( Location location )
	{
		return dispensers.containsKey( location );
	}

	
	/**
	 * NOTE: See if this is necessary. I really would like to not break encapsulation here.
	 * @return
	 */
	@Override
	public Map<Location, DispenserState> getData()
	{
		return dispensers;
	}
	
	
	@Override
	public boolean isEmpty()
	{
		return dispensers.isEmpty();
	}
	
	
	private void writeToDisk(JSONObject data) {
		
		if (filePath == null) {
			throw new NullPointerException("A file path must be set before saving.");
		}
		
		try {
    		FileWriter file = new FileWriter(filePath);
    		file.write( data.toJSONString() );
    		file.flush();
    		file.close();
    	} catch (IOException ex) {
    		ex.printStackTrace();
    	}
		
	}
	
	private DispenserState extractDispenserState( JSONObject dispenserState )
	{
		DispenserAction type = DispenserAction.convertToDispenserAction( (String) dispenserState.get( "type" ));
		DispenserAction currentAction = DispenserAction.convertToDispenserAction( (String) dispenserState.get( "current action" ));
		DispenserState dispState = new DispenserState( type, currentAction );
		return dispState;
	}

	private Location extractAndCreateLocation( JSONObject dispenserState )
	{
		double x;
		double y;
		double z;
		String worldName;
		JSONObject dispenserLocation = (JSONObject) dispenserState.get( "location" );
		x = (Double) dispenserLocation.get( "x" );
		y = (Double) dispenserLocation.get( "y" );
		z = (Double) dispenserLocation.get( "z" );
		worldName = (String) dispenserLocation.get( "world" );
		Location loc = new Location(Bukkit.getServer().getWorld( worldName ), x, y, z);
		return loc;
	}
}
