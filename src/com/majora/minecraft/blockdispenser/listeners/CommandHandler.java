/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.listeners;

import org.bukkit.block.Dispenser;
import org.bukkit.entity.Player;

import com.majora.minecraft.blockdispenser.BlockDispenser;
import com.majora.minecraft.blockdispenser.DispenserAction;
import com.majora.minecraft.blockdispenser.DispenserState;
import com.majora.minecraft.blockdispenser.models.IRepository;
import com.majora.minecraft.blockdispenser.utils.Utility;

/**
 * @author jvmilazz
 *
 */
public final class CommandHandler
{
	public static void handleSetCommand(final Player player, IRepository repository, String setType, BlockDispenser plugin) 
	{
		
		Dispenser selectedDispenser = (Dispenser) Utility.getMetadata( player, "bd_selected_dispenser", plugin );
		
		if (selectedDispenser == null) 
		{
			player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.DispenserNotSet" ));
			return;
		}
		
		DispenserState state;
		if (setType.equalsIgnoreCase( "eject" ))
		{
			state = new DispenserState(DispenserAction.EJECT, DispenserAction.EJECT);
		} else if (setType.equalsIgnoreCase( "remove" ))
		{
			state = new DispenserState(DispenserAction.REMOVE, DispenserAction.REMOVE);
		} else if (setType.equalsIgnoreCase( "normal" ))
		{
			state = new DispenserState(DispenserAction.NORMAL, DispenserAction.NORMAL);
		} else if (setType.equalsIgnoreCase( "both" ) || setType.equalsIgnoreCase( "eject-remove" ))
		{
			state = new DispenserState(DispenserAction.BOTH, DispenserAction.EJECT);
		} else {
			player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.DispenserSetError" ) );
			return;
		}
		
		repository.put( selectedDispenser.getLocation(), state );
		
		String temp = plugin.getLocale().getValue( "Command.DispenserSet" );
		player.sendMessage( BlockDispenser.prefix + plugin.getLocale().format( temp, setType ) );

	}
	
	public static void handleForceSaveCommand(final Player player, IRepository repository, BlockDispenser plugin)
	{
		repository.save();
		player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.ForceSave" ));
	}
	
	public static void handleStateCommand(final Player player, IRepository repository, BlockDispenser plugin) 
	{
		Dispenser selectedDispenser = (Dispenser) Utility.getMetadata( player, "bd_selected_dispenser", plugin );
		
		if (selectedDispenser == null) 
		{
			player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.DispenserNotSet" ));
			return;
		}
		
		DispenserState dispState = (DispenserState) repository.get( selectedDispenser.getLocation() );
		
		String message = "";
		if (dispState.getDispenserType() == DispenserAction.BOTH)
		{
			message = plugin.getLocale().format( "Dispenser State is: [{} : {}].", dispState.getDispenserType().name(), dispState.getCurrentState().name() );
		} else 
		{
			message = plugin.getLocale().format( "Dispenser State is: [{}].", dispState.getDispenserType().name() );
		}
		
		player.sendMessage( BlockDispenser.prefix + message );
	}
	
	public static void handleReloadDispensers(final Player player, IRepository repository, BlockDispenser plugin)
	{
		repository.save();
		player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.ReloadDispenserSaved" ) );
		repository.load();
		player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.ReloadDispenserEnd" ) );
	}
	
	public static void handleReloadConfig(final Player player, IRepository repository, BlockDispenser plugin)
	{
		plugin.reloadConfig();
		player.sendMessage( BlockDispenser.prefix + plugin.getLocale().getValue( "Command.ReloadConfigEnd" ));
	}
}
