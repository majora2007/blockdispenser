/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.listeners;

import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.yi.acru.bukkit.Lockette.Lockette;

import com.majora.minecraft.blockdispenser.BlockDispenser;
import com.majora.minecraft.blockdispenser.DispenserAction;
import com.majora.minecraft.blockdispenser.DispenserState;
import com.majora.minecraft.blockdispenser.models.IRepository;
import com.majora.minecraft.blockdispenser.utils.Authentication;
import com.majora.minecraft.blockdispenser.utils.Utility;

public class PlayerListener implements Listener
{
	private BlockDispenser plugin;
	private IRepository<Location, DispenserState> repository;
	
	public PlayerListener(BlockDispenser instance, IRepository repository) {
		this.plugin = instance;
		this.repository = repository;
	}
	
	/**
	 * When player right clicks a Dispenser, "select" it.
	 * 
	 * @param event
	 */
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent event)
	{
		if(!event.hasBlock()) return;
		
		if (isRightClick( event ) && isClickedBlockDispenser( event ))
		{
			Dispenser prevSelectedDispenser = (Dispenser) Utility.getMetadata( event.getPlayer(), "bd_selected_dispenser", this.plugin );
			Dispenser selectedDispenser = (Dispenser) event.getClickedBlock().getState();
			
			if (this.plugin.getConfig().getBoolean( "options.lockette" ) && Lockette.isProtected( event.getClickedBlock() ))
			{
				// If clicking Player is either an owner or an accepted modifier of the protected block
				if (Lockette.isUser( event.getClickedBlock(), event.getPlayer().getName(), true ))
				{
					Utility.setMetadata( event.getPlayer(), "bd_selected_dispenser", selectedDispenser, this.plugin );
				} else {
					this.plugin.getLogger().log( Level.INFO, "You do not have permission to modify Dispenser." );
					return;
				}
			} else {
				Utility.setMetadata( event.getPlayer(), "bd_selected_dispenser", selectedDispenser, this.plugin );
			}
			
			if ( isFirstSelection(prevSelectedDispenser, selectedDispenser) )
			{
				event.getPlayer().sendMessage( plugin.color("green", BlockDispenser.prefix + plugin.getLocale().getValue( "Command.DispenserSelected" )) );
			}
			
			addDispenser( selectedDispenser );
		}
	}

	
	@EventHandler
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event)
	{
		if (event.isCancelled()) return;
		
		String[] command = event.getMessage().split(" ", 3);

		if(command.length < 1) return;
		
		if(!(command[0].equalsIgnoreCase("/blockdispenser") || command[0].equalsIgnoreCase("/bd"))) return;
		
		event.setCancelled(true);

		Player player = event.getPlayer();
		
		// Set a dispenser's state
		if (command.length == 3)
		{
			if (command[1].equalsIgnoreCase( "set" ) && Authentication.hasPermission( player, "blockdispenser.set" )) 
			{
				CommandHandler.handleSetCommand(player, repository, command[2], this.plugin);
				return;
			} else if (command[1].equalsIgnoreCase( "reload" ) && Authentication.hasPermission( player, "blockdispenser.reload" ))
			{
				if (command[2].equalsIgnoreCase( "dispensers" ) || command[2].equalsIgnoreCase( "dispenser" ))
				{
					CommandHandler.handleReloadDispensers( player, repository, plugin );
				} else if (command[2].equalsIgnoreCase( "config" ))
				{
					CommandHandler.handleReloadConfig( player, repository, plugin );
				}
			}
		}
		
		// force-save
		if (command.length == 2)
		{
			if (command[1].equalsIgnoreCase( "force-save" ) && Authentication.hasPermission( player, "blockdispenser.force-save" ))
			{
				CommandHandler.handleForceSaveCommand(player, repository, this.plugin);
			} else if (command[1].equalsIgnoreCase( "state" ) && Authentication.hasPermission( player, "blockdispenser.state" ))
			{
				CommandHandler.handleStateCommand(player, repository, this.plugin);
			}
		}
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event)
	{
		Block mBlock = event.getBlock();
		if (mBlock.getType() == Material.DISPENSER)
		{
			Dispenser d = (Dispenser) mBlock.getState();
			addDispenser(d);
		}
	}
	
	
	private void addDispenser(Dispenser dispenser)
	{
		// Add the dispenser and give it default DispenserAction.NORMAL
		if (!repository.containsKey( dispenser.getLocation() ))
		{
			DispenserState state = new DispenserState( DispenserAction.NORMAL, DispenserAction.NORMAL );
			
			repository.put( dispenser.getLocation(), state );
		}
	}

	private boolean isClickedBlockDispenser( PlayerInteractEvent event )
	{
		return event.getClickedBlock().getType() == Material.DISPENSER;
	}

	private boolean isRightClick( PlayerInteractEvent event )
	{
		return (event.getAction() == Action.RIGHT_CLICK_BLOCK);
	}
	
	private boolean isFirstSelection(Dispenser prevSelectedDispenser, Dispenser selectedDispenser)
	{
		return (prevSelectedDispenser == null || (!prevSelectedDispenser.equals( selectedDispenser )));
	}
}
