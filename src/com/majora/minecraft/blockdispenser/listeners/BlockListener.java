/**
 * Distributed under MIT License
 * http://www.opensource.org/licenses/MIT
 */
package com.majora.minecraft.blockdispenser.listeners;

import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Dispenser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import com.majora.minecraft.blockdispenser.BlockDispenser;
import com.majora.minecraft.blockdispenser.DispenserAction;
import com.majora.minecraft.blockdispenser.DispenserObject;
import com.majora.minecraft.blockdispenser.DispenserState;
import com.majora.minecraft.blockdispenser.models.IRepository;
import com.majora.minecraft.blockdispenser.utils.MaterialHelper;



public class BlockListener implements Listener {

	/**
	 * 
	 */
	private static final int	BONEMEAL	= 351;
	/**
	 * 
	 */
	private static final byte	FULLY_GROWN_CROP	= (byte) 7;
	
	private BlockDispenser plugin;
	private IRepository<Location, DispenserState> repository;
	
	private boolean debug = false;
	private boolean success = false;

	public BlockListener(BlockDispenser instance, IRepository repository) {
		plugin = instance;
		this.repository = repository;
		debug = plugin.isDebug();
	}
	

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockDispense(BlockDispenseEvent event) 
	{
		if (success)
		{
			event.setCancelled(true);
			success = false;
		}
		
		if (debug && event.isCancelled())
		{
			BlockDispenser.log( "BlockDispense event caught!", null );
		}
	}
	

	/* (non-Javadoc)
	 * @see org.bukkit.event.block.BlockListener#onBlockRedstoneChange(org.bukkit.event.block.BlockRedstoneEvent)
	 */
	@EventHandler
	public void onBlockRedstoneChange(BlockRedstoneEvent event) 
	{
		if ( isRedstoneSignalRising( event ))
		{
			Block poweredBlock = event.getBlock();
			
			DispenserObject dispenserWithFace = searchSurroundingBlocksForDispenser(poweredBlock);
			
			if (dispenserWithFace.getDispenser() != null)
			{
				Dispenser dispenser = dispenserWithFace.getDispenser();
				
				addDispenser( dispenser ); 
					
				// If DispenserAction.NORMAL, return
				if (isNormalDispenser( dispenser ))
				{	
					success = dispenser.dispense();
					return;
				}
				
				ItemStack itemToDespense = findNextItemToDespense( dispenser );
				Block targetBlock = dispenser.getBlock().getRelative( dispenserWithFace.getDispenserFace(), 1 );
				
				// Handle Dispenser
				handleDispenser(dispenser, targetBlock, itemToDespense);

			}
			
		}
	}


	private boolean isNormalDispenser( Dispenser dispenser )
	{
		return repository.get( dispenser.getLocation() ).getDispenserType() == DispenserAction.NORMAL;
	}


	private void addDispenser( Dispenser dispenser )
	{
		// Add the dispenser and give it default DispenserAction.NORMAL
		if (!repository.containsKey( dispenser.getLocation() ))
		{
			DispenserState state = new DispenserState( DispenserAction.NORMAL, DispenserAction.NORMAL );
			
			repository.put(dispenser.getLocation(), state);
		}
	}

	private ItemStack findNextItemToDespense( Dispenser dispenser )
	{
		ItemStack item = new ItemStack(0); // The actual ItemStack that will be taken, also defaults to nothing.
		
		// Finds the next item type to dispense
		for (ItemStack i : dispenser.getInventory().getContents()) 
		{ 
			if (i != null) 
			{
				if (i.getTypeId() != 0)
				{
					item = i;
					break;
				}
			}
		}
		return item;
	}
	
	private DispenserObject searchSurroundingBlocksForDispenser( Block poweredBlock )
	{
		DispenserObject dispenserObj = new DispenserObject();
		
		Dispenser dispenser = null;
		int xDirection, yDirection, zDirection;
		Location poweredBlockLocation = poweredBlock.getLocation();
		
		for (int _x = -1; _x <= 1; _x++)
		{
			for (int _y = -1; _y < 1; _y++)
			{
				for (int _z = -1; _z <= 1; _z++)
				{
					if (_z + _x == 1 || _z + _x == -1)
					{
						if (poweredBlock.getWorld().getBlockAt(poweredBlockLocation.getBlockX() + _x, poweredBlockLocation.getBlockY() + _y, poweredBlockLocation.getBlockZ() + _z).getState().getType() == org.bukkit.Material.DIODE) {
							BlockDispenser.log("Hi", null);
						}
						
						if (poweredBlock.getWorld().getBlockAt(poweredBlockLocation.getBlockX() + _x, poweredBlockLocation.getBlockY() + _y, poweredBlockLocation.getBlockZ() + _z).getState().getType() == org.bukkit.Material.DISPENSER) 
						{
							dispenser = (Dispenser) poweredBlock.getWorld().getBlockAt(poweredBlockLocation.getBlockX() + _x, poweredBlockLocation.getBlockY() + _y, poweredBlockLocation.getBlockZ() + _z).getState();
							
							xDirection = _x;
							yDirection = _y;
							zDirection = _z;
							dispenserObj.setDispenser( dispenser );
							org.bukkit.material.Dispenser dispenserMaterial = (org.bukkit.material.Dispenser) poweredBlock.getWorld().getBlockAt(poweredBlockLocation.getBlockX() + xDirection, poweredBlockLocation.getBlockY() + yDirection, poweredBlockLocation.getBlockZ() + zDirection).getState().getData(); //Used for the "getFacing()" method
							dispenserObj.setDispenserFace( dispenserMaterial.getFacing() );
						}
					}
				}
			}
		}
		
		return dispenserObj;
	}

	private boolean isRedstoneSignalRising( BlockRedstoneEvent event )
	{
		return (event.getOldCurrent() == 0 && event.getNewCurrent() >= 1);
	}
	
	
	/**
	 * Removes a given <code>ItemStack</code> from a <code>Dispenser</code>.
	 * 
	 * @param dispenser
	 * @param item
	 * @see Dispenser
	 * @See ItemStack
	 */
	private void removeItem(InventoryHolder dispenser, ItemStack item)
	{
		if (item == null || MaterialHelper.isAir( item.getType() )) return;
		
		int slot = dispenser.getInventory().first(item.getType());
		ItemStack preStack = dispenser.getInventory().getItem(slot);
		
		if (preStack.getAmount() <= 1)
		{
//			if (item.getType() == Material.REDSTONE_TORCH_ON) // QUESTION IS this still neccessary? torches are now just dispensed
//			{
//				log.info( BlockDispenser.TAG + "REDSTONE TORCH IS BEING HANDLED." );
//				preStack.setAmount(0);
//				dispenser.getInventory().setItem(slot, preStack);
//			} else 
			{
				preStack.setAmount(0);
				dispenser.getInventory().setItem(slot, preStack);
			}
		}
		else
		{
			preStack.setAmount(preStack.getAmount() - 1);
			dispenser.getInventory().setItem(slot, preStack);
		}
	}
	
	/**
	 * Adds a given <code>ItemStack</code> to a dispenser and removes the <code>item</code> from the <code>Block</code> it resides on.
	 * 
	 * 
	 * @param dispenser
	 * @param item
	 * @param amount
	 * @return Whether <code>ItemStack</code> was added to dispenser or not.
	 * 
	 * @see Dispenser
	 * @See ItemStack
	 */
	private boolean addItem(Dispenser dispenser, Block targetBlock, ItemStack item)
	{
		try {
			if (dispenser.getInventory().addItem(item).isEmpty())
			{
				if (debug)
				{
					BlockDispenser.log( "Removing " + targetBlock.getType().name(), null );
				}
				
				// Set targetBlock to AIR and force BlockUpdate
				targetBlock.setTypeId(0, true);
				
				return true;
				
			} else if (debug) 
			{
				BlockDispenser.logSevere( "Dispenser at " + dispenser.getX() + "," + dispenser.getY() + "," + dispenser.getZ() + " is full!", "red" );
				//log.severe("Dispenser at " + dispenser.getX() + "," + dispenser.getY() + "," + dispenser.getZ() + " is full!");
				return false;
			}
		} catch (Exception ex) 
		{
			//log.severe(e.getMessage());
			BlockDispenser.logSevere( ex.getMessage(), "red" );
			
		}
		
		return false;
	}
	
	/**
	 * Fills stacks to max size in order to save space in <code>Dispenser</code>'s <code>Inventory</code>.
	 * 
	 * @param dispenser
	 */
	@SuppressWarnings( value = { "unused" } )
	private void consolidateDispenserInv(Dispenser dispenser)
	{
		// TODO Implement this method...
	}
	
	
	private void handleDispenser(Dispenser dispenser, Block targetBlock, ItemStack item)
	{
		DispenserState dispenserState = repository.get( dispenser.getLocation() );
		success = false;
		
		
		if ( dispenserState.getDispenserType() == DispenserAction.EJECT )
		{	

			handleEjectDispenser( dispenser, targetBlock, item );
			
		} else if (dispenserState.getDispenserType() == DispenserAction.REMOVE)
		{
			if (debug)
			{
				BlockDispenser.log( "The targetBlock to remove is: " + targetBlock.getType().name(), null );
			}
			
			if (isInventoryHolder( targetBlock ))
			{
				removeItemFromContainer( dispenser, targetBlock );
				
			} else if (targetBlock.getTypeId() != 0)
			{
				// Remove block and add it to inventory
				item = new ItemStack( targetBlock.getTypeId(), 1, targetBlock.getData() );
				addItem(dispenser, targetBlock, item);
			}
			success = true; // REMOVE should always cancel event because if it can't suck, it doesn't.
			return;
		} else if (dispenserState.getDispenserType() == DispenserAction.BOTH) 
		{
			if (dispenserState.getCurrentState().equals( DispenserAction.EJECT ))
			{
				handleEjectDispenser( dispenser, targetBlock, item );
//				if (item.getType() == Material.REDSTONE_TORCH_ON) {
//					success = false;
//					return;
//				}
//				
//				if (item.getTypeId() == BONEMEAL && MaterialHelper.canUseBonemealOn( targetBlock.getType() )) 
//				{
//					if (notGrown( targetBlock )) // This will not work on sapplings.
//					{
//						removeItem(dispenser, item);
//						targetBlock.setData( FULLY_GROWN_CROP );
//						success = true; // True because we have used up the item
//						return;
//					}
//				} else if (isInventoryHolder( targetBlock ))
//				{
//					ejectItemIntoContainer( dispenser, targetBlock, item );
//					
//					success = true;
//					
//				} else if (MaterialHelper.isEmptyBlock( targetBlock.getType() ))
//				{
//					success = placeBlock(dispenser, targetBlock, item);
//					
//					if (!success)
//					{
//						success = dispenser.dispense();
//					}
//				}
//				
				if (success)
				{
					dispenserState.toggleState();
					repository.put(dispenser.getLocation(), dispenserState);
				}
			} else if (dispenserState.getCurrentState().equals( DispenserAction.REMOVE ))
			{
				if (isInventoryHolder( targetBlock ))
				{
					removeItemFromContainer( dispenser, targetBlock );
					success = true;
				} else if (targetBlock.getTypeId() != 0)
				{
					// Remove block and add it to inventory
					item = new ItemStack( targetBlock.getTypeId(), 1, targetBlock.getData() );
					success = addItem(dispenser, targetBlock, item);
				}
				
				if (success)
				{
					dispenserState.toggleState();
					repository.put( dispenser.getLocation(), dispenserState );
				}
				return;
			}
			
		}
	}


	private void handleEjectDispenser( Dispenser dispenser, Block targetBlock, ItemStack item )
	{
		if (item.getType() == Material.REDSTONE_TORCH_ON) {
			success = false;
		} else if (item.getTypeId() == BONEMEAL && MaterialHelper.canUseBonemealOn( targetBlock.getType() )) 
		{
			if (notGrown( targetBlock )) // This will not work on sapplings.
			{
				removeItem(dispenser, item);
				targetBlock.setData( FULLY_GROWN_CROP );
				success = true; // True because we have used up the item
			}
		}
		else if (MaterialHelper.isEmptyBlock( targetBlock.getType() )) {
			// try to placeBlock
			success = placeBlock(dispenser, targetBlock, item);
			
			// if it doesn't work, try to dispense naturally
			if (!success)
			{
				success = dispenser.dispense();
			}
		} else if (isInventoryHolder( targetBlock ))
		{
			ejectItemIntoContainer( dispenser, targetBlock, item );
		}
		
		
	}


	private boolean notGrown( Block targetBlock )
	{
		return targetBlock.getData() < 7;
	}


	private void removeItemFromContainer( Dispenser dispenser, Block targetBlock )
	{
		InventoryHolder container = (InventoryHolder) targetBlock.getState();
		
		ItemStack itemToRemove = findFirstItemInInventory( container );
		
		if (itemToRemove != null)
		{
			addItemToContainer( itemToRemove, dispenser );
			removeItem( container, itemToRemove );
		}
	}


	private void ejectItemIntoContainer( Dispenser dispenser,
			Block targetBlock, ItemStack item )
	{
		InventoryHolder container = (InventoryHolder) targetBlock.getState();
		addItemToContainer( item, container );
		removeItem( dispenser, item );
	}


	private ItemStack findFirstItemInInventory( InventoryHolder container )
	{
		ItemStack firstItem = null;
		for (int i = 0; i < container.getInventory().getSize(); i++)
		{
			firstItem = container.getInventory().getItem( i );
			if (firstItem != null && firstItem.getAmount() > 0) break;
		}
		return firstItem;
	}


	private boolean isInventoryHolder( Block targetBlock )
	{
		return targetBlock.getState() instanceof InventoryHolder;
	}


	private void addItemToContainer( ItemStack item, InventoryHolder container )
	{
		if (item.getAmount() <= 0 || MaterialHelper.isAir( item.getType() )) return;
		
		ItemStack oneItem = item.clone();
		oneItem.setAmount( 1 );
		
		container.getInventory().addItem( oneItem );
	}
	
	/**
	 * Place a block from a dispenser. </p> This is used to allow specific handling to occur in one place.
	 * 
	 * @param dispenser		The <code>Dispenser</code> to take block from.
	 * @param targetBlock				The <code>Block</code> to place onto.
	 * @param item			The <code>ItemStack</code> which is being manipulated.
	 */
	private boolean placeBlock(Dispenser dispenser, Block targetBlock, ItemStack item)
	{
		// TODO: BUG: This is where the rails will place over existing rails.
		int itemID = item.getTypeId();
		
		if ( (itemID < 256 || MaterialHelper.isSpecialPlaceables(itemID)) && itemID > Material.AIR.getId() /*!MaterialHelper.isAir( itemID )*/ )
		{
		
			switch (itemID)
			{
				case 328: // Minecart
					targetBlock.getWorld().spawn(targetBlock.getLocation(), org.bukkit.entity.Minecart.class); // Dispenses the minecart
					dispenser.getInventory().removeItem(item);
					break;
				case 342: // Storage Minecart
					targetBlock.getWorld().spawn(targetBlock.getLocation(), org.bukkit.entity.StorageMinecart.class); // Dispenses the minecart
					dispenser.getInventory().removeItem(item);
					break;
				case 343: // Powered Minecart
					targetBlock.getWorld().spawn(targetBlock.getLocation(), org.bukkit.entity.PoweredMinecart.class); // Dispenses the minecart
					dispenser.getInventory().removeItem(item);
					break;
				default:
					if (item.getTypeId() >= 256 && !MaterialHelper.isAir( itemID ) )
					{
						return false;
					} else
					{
						// TODO: BUG: Figure out how to fix this issue. Redstone torches should be placed on the target block (standing), not attached to dispenser.
						if (targetBlock.getType() == Material.REDSTONE_TORCH_ON)
						{
							this.plugin.getLogger().log( Level.INFO, "Placing Redstone torch" );
							
							//targetBlock.setTypeIdAndData(Material.REDSTONE_TORCH_ON.getId(), (byte ) 6, true); // Stand straight on block
							return false; // Cannot find a fix to this, dispense normally.
						} else {
							targetBlock.setTypeIdAndData(itemID, (byte) item.getDurability(), true); //Dispenses the block
						}
						
						this.removeItem(dispenser, item);
					}
					break;
			}
			return true;
		} 	
		return false;
	}
}
